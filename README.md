# Hardware:

- Processor: Intel Core i5-10400F (Comet Lake)
- Memory: 2 x 8GB Crucial Ballistix DDR4-3000MHz (Total: 16GB)
- Motherboard: Gigabyte Z490M Gaming X - BIOS: F21
- Graphics Card: AMD RX550 4GB (Lexa Core)
- Storage: 480GB Crucial BX500 SSD
- Wireless: TP-Link Archer T2U Nano USB

# Bootloader
OpenCore v0.7.6-RELEASE

# Operating System
![About this Mac](https://user-content.gitlab-static.net/833bc6d05ba4c8da2790a36c77b96bad27552161/68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f31363531393735382f3134373833343739372d62623661663133662d383665662d343364622d616539382d3461623439303538383664372e706e67)

## BIOS Settings

**Disabled:**

- Fast Boot
- VT-d
- CSM
- Intel SGX
- Intel Platform Trust
- Trusted Platform Module
- CFG Lock (MSR 0xE2 write protection)
- Secure Boot

**Enabled:**

- VT-x
- Above 4G decoding
- Hyper-Threading
- Execute Disable Bit
- EHCI/XHCI Hand-off
- OS type: Other OS


# Highlights:
- Installation procedure is 99.9% based on OpenCore's guide: https://dortania.github.io/OpenCore-Install-Guide

- SIP + GateKeeper is disabled; it makes things WAAAAAAAAY easier to set-up.

- Graphics Card doesn't work out of the box (Explained here: https://github.com/dortania/bugtracker/issues/129 ), however there's a trick to spoof the device ID and make the OS recognizes it as a RX 560 of some sort. Multiple monitors, UI effects like animations, transparency and HW-Accelerated tasks with Adobe software, Wondershare Filmora works fine. How to do it here: https://ipv6.reddit.com/r/hackintosh/comments/lnadiw/comment/go6kjpy/?context=3

- Wireless: AirDrop, Hand-off, Continuity, Sidecar etc. doesn't work.
Driver installation process here: https://github.com/chris1111/Wireless-USB-OC-Big-Sur-Adapter

- USB ports were mapped according to the following scheme - Case uses 2 x USB2 ports + 1 x USB3 port
![Hackintool - In use USB ports](https://user-content.gitlab-static.net/95569286e7c81d3314621430f7c268d05dc48a64/68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f31363531393735382f3134373833343738372d33366634656139312d366265332d343261392d626531662d6236633436613834613362372e706e67)


Screenshots (System Report):
![System Report - Audio](https://user-content.gitlab-static.net/25b0db4d238a645ad4f025415002a1260ec5abfd/68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f31363531393735382f3134373833343830372d37336131313332662d393631342d343962382d623838612d3831363565613536633563612e706e67)
![System Report - Graphics](https://user-content.gitlab-static.net/7681e48c074713042849fe2a47ab8e7ea52d5666/68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f31363531393735382f3134373833343831312d33306437303066332d663334642d346666622d386461362d6233326137303762373233642e706e67)
![System Report - Network](https://user-content.gitlab-static.net/08afdb144f3a3eae5afcaeed7e22a0b0a6011074/68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f31363531393735382f3134373833343831322d37393561613137372d313130332d346131372d393634342d3536376336306561393635302e706e67)
![System Report - SATA](https://user-content.gitlab-static.net/43699b7119782fd161225b33a23e1604255c19da/68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f31363531393735382f3134373833343939362d37306635393039612d373966392d346263342d386665362d6635366633646164366437342e706e67)
![System Report - USB](https://user-content.gitlab-static.net/4c63a8105f1b7d26acaae5277ccccdc2944096db/68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f31363531393735382f3134373833343831342d37643134663739302d663833652d343865332d616136382d3166643436316337306361662e706e67)
